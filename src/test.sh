#!/bin/bash

test_number=0
errors=0

test_number=$((test_number+1))
echo "2" | ./build/code
if [ $? -eq 1 ] #если это не простое число, то ошибка
then
echo "ERORR: Job failed: Test $test_number"
errors=1
fi

test_number=$((test_number+1))
echo "82" | ./build/code
if [ $? -eq 0 ] #если это простое число, то ошибка
then
echo "ERORR: Job failed: Test $test_number"
errors=1
fi

test_number=$((test_number+1))
echo "1000" | ./build/code
if [ $? -eq 0 ] #если это простое число, то ошибка
then
echo "ERORR: Job failed: Test $test_number"
errors=1
fi

test_number=$((test_number+1))
echo "139" | ./build/code
if [ $? -eq 1 ] #если это не простое число, то ошибка
then
echo "ERORR: Job failed: Test $test_number"
errors=1
fi

test_number=$((test_number+1))
echo "61" | ./build/code
if [ $? -eq 1 ] #если это не простое число, то ошибка
then
echo "ERORR: Job failed: Test $test_number"
errors=1
fi

if [ $errors -eq 1 ]
then
exit 1
fi 
