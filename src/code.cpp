#include <iostream>

bool isPrime(int n) {
if (n <= 1) {
return false;
}
for (int i = 2; i * i <= n; ++i) {
if (n % i == 0) {
return false;
}
}
return true;
}

int main(int argc, char *argv[]) {
setlocale(LC_ALL, "Russian");
int n;
if (argc == 2){
std::string a = argv[1];
n = std::stoi(a);
}
else{
std::cout << "Введите число для проверки: ";
std::cin >> n;
}

if (isPrime(n)) {
std::cout << n << " является простым числом." << std::endl;
return 0;
}
else {
std::cout << n << " не является простым числом." << std::endl;
return 1;
}
}
