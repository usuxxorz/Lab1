FROM ubuntu:latest

WORKDIR /tmp

COPY ./*.deb /tmp/

RUN apt-get upgrade; apt-get update; apt-get install -y /tmp/package.deb;

CMD ["bash"]
